<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/hello', function () {
//     return '<h1>Hello World</h1>';
// });

// Route::get('/user/{id}/{name}', function ($id, $name) {
//     return 'The User Name is '.$name. 'and the user ID '.$id;
// });

// Route::get('/', function () {
//     return view('welcome');
// });

// ################ This Format can't Pass some Data ################
// Route::view('/', 'index');
// Route::view('/index', 'index'); 
// Route::view('/touroperator', 'touroperator'); //Tour Operator
// Route::view('/about', 'about');
// Route::view('/services', 'services');
// Route::view('/login', 'login');
// Route::view('/signup', 'signup');
// Route::view('/admin', 'admin');
// Route::view('/contactus', 'contactus'); //PagesController@
// Route::view('/backup', 'backup');

// ################ PAGE CONTROL ####################
Route::get('/', 'PagesController@index'); //of url [Home for Index] view #####DONE#####
Route::get('index', 'PagesController@index'); //of url [index] view #####DONE#####
Route::get('about', 'PagesController@about'); //of url [about] view #####DONE#####
Route::get('services', 'PagesController@services'); //of url [services] view #####DONE#####
Route::get('connect/login', 'PagesController@login'); //of url [login] view #####DONE#####
Route::get('signup', 'PagesController@signup'); //of url [signup] view #####DONE#####
Route::get('manager', 'PagesController@manager'); //of url [admin] view #####DONE#####
Route::get('contactus', 'PagesController@contactus'); //of url [contactus] view #####DONE#####
Route::get('touroperator', 'PagesController@touroperator'); //of url [touroperator] view #####DONE#####
Route::get('touroperator/index', 'PagesController@touroperator'); //of url [touroperator] view #####DONE#####

// ################ TOUR PACKAGE CONTROLLER ####################
Route::get('touroperator', 'TourPackageController@tourpackageslist'); //this method TourPackageController will return information of the function tourpackageslist()
Route::get('touroperator/index', 'TourPackageController@tourpackageslist'); //this method TourPackageController will return information of the function tourpackageslist()
// Route::get('touroperator/create', 'TourPackageController@create'); 
Route::post('touroperator', 'TourPackageController@store'); //this method TourPackageController will push information of the function store() to database
Route::get('touroperator/{id}', 'TourPackageController@show');
Route::get('touroperator/{id}/edit', 'TourPackageController@edit');
Route::post('touroperator/{id}', 'TourPackageController@update');
Route::delete('touroperator/{id}', 'TourPackageController@destroy');

// ################ TRAVEL AGENT CONTROLLER ####################
Route::resource('/', 'TravelAgentController'); //this method will show the information of the TourPackageController with the function of index()
Route::resource('index', 'TravelAgentController'); //this method will show the information of the TourPackageController with the function of index()
Route::get('/travelagent/{id}', 'TravelAgentController@show');

// ################ PASSING DATA TO URL && VIEW TOUROPERATOR PAGE ###############
// Route::get('touroperator', 'TourPackageController@tourpackageslist');
// Route::get('index', 'TourPackageController@tourpackageslist');

// Route::get('index/{id}', 'TourPackageController@show');

// Route::get('index/{id}/{name}', function ($id, $name) {
//     return 'The User Name is '.$name. ' and the user ID '.$id;
// });

// ############### STORE DATA / SAVE DATA TO DATABASE ##################
// Route::post('touroperator', 'TourPackageController@store');
// Route::post('touroperator', 'TourPackageController@update');

// Route::post('index2', function(){
//     dd(Input::has('adult'));

//     $adult = Input::has('adult') ? true : false;
// });

//view details of tourpackage
// // Route::resource('touroperator/edit', 'TourPackageController');
// Route::resource('show', 'TourPackageController');
// Route::resource('index', 'TourPackageController');
// Route::resource('/', 'TourPackageController');

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
