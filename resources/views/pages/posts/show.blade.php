@extends('layouts.app')

@section('content')
  <a href="/show" class="btn btn-default">Go Back</a>
  <h1>Tour Package Detail</h1>
  <br>
  <h3>{{$viewtourpackages->tp_name}}</h3>
  <div>
    {{$viewtourpackages->tp_description}}
  </div>
  <hr>
    <small>Written on {{$viewtourpackages->created_at}}</small>
@endsection