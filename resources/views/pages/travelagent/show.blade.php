@extends('layouts.app')

@section('content')
<h1>Travel Agent/Show ({{$viewtourpackages->id}})</h1>
<div class="container">
  <div class="row">
    <div class="col">
      <div class="card">
        <div class="card-header">
          Featured
        </div>
          <img src="https://via.placeholder.com/800x500" class="rounded float-left" alt="...">
        <div class="card-body">
          <div class="row">
            <div class="col">
              <h2 class="card-title">{{$viewtourpackages->tp_name}}</h2>
            </div>
            <div class="col">
              <h4>Rating * * * * *{{$viewtourpackages->tp_rating}}</h4>
              <h4>P{{$viewtourpackages->tp_price}}</h4>
            </div>
          </div>
          <p class="card-text">{{$viewtourpackages->tp_description}}</p>
        </div>
        <ul class="list-group list-group-flush">
          <li class="list-group-item">
            <div class="row">
              <div class="col">Start Date: {{$viewtourpackages->tp_start}}</div>
              <div class="col">End Date: {{$viewtourpackages->tp_end}}</div>
            </div>
            <div class="row">
              <div class="col">Departure Date: {{$viewtourpackages->tp_departure}}</div>
              <div class="col">Destination Date: {{$viewtourpackages->tp_destination}}</div>
            </div>
          </li>
          <li class="list-group-item">Dapibus ac facilisis in</li>
          <li class="list-group-item">Vestibulum at eros</li>
        </ul>
        <div class="card-body">
          
        </div>
      </div>
    </div>
    <div class="col-3">
      <div class="row">
        <ul class="list-group">
          <li class="list-group-item">
            <div class="row">
              <div class="col-3">Rating</div>
              <div class="col">
                <input type="text" name="tp_rating" class="form-control" id="formGroupTourPackageSettingInput1" placeholder="(1-5)">
              </div>
            </div>
            <div class="row">
              <div class="col-3">Day</div>
              <div class="col">
                <input type="text" name="tp_day" class="form-control" id="formGroupTourPackageSettingInput2" placeholder="Day">
              </div>
            </div>
            <div class="row">
              <div class="col-3">Night</div>
              <div class="col">
                <input type="text" name="tp_night" class="form-control" id="formGroupTourPackageSettingInput3" placeholder="Night">
              </div>
            </div>
            <div class="row">
              <div class="col-3">VAT</div>
              <div class="col">
                <input type="text" name="tp_vat" class="form-control" id="formGroupTourPackageSettingInput4" placeholder="Vat">
              </div>
            </div>
            <div class="row">
              <div class="col-3">Tax</div>
              <div class="col">
                <input type="text" name="tp_tax" class="form-control" id="formGroupTourPackageSettingInput5" placeholder="Tax">
              </div>
            </div>
          </li>
          <li class="list-group-item">
            <div class="row">
              <div class="col">Location</div>
              <div class="col">Check</div>
            </div>
            <div class="row">
              <div class="col">Inclusion</div>
              <div class="col">Check</div>
            </div>
            <div class="row">
              <div class="col">Exclusion</div>
              <div class="col">Check</div>
            </div>
            <div class="row">
              <div class="col">Payment And Policy</div>
              <div class="col">Check</div>
            </div>
            <div class="row">
              <div class="col">Contact</div>
              <div class="col">Check</div>
            </div>
          </li>
          <li class="list-group-item">
            <div class="row">
              <h4>Payment Detials</h4>
            </div>
            <div class="row">
              <div class="col">Total Stay (No.)</div>
              <div class="col">Check</div>
            </div>
            <div class="row">
              <div class="col">VAT</div>
              <div class="col">Check</div>
            </div>
            <div class="row">
              <div class="col">Tax</div>
              <div class="col">Check</div>
            </div>
            <div class="row">
              <div class="col">Inclusion</div>
              <div class="col">Check</div>
            </div>
            <div class="row">
              <div class="col">Exclusion</div>
              <div class="col">Check</div>
            </div>
          </li>
          <li class="list-group-item">
            <h4>P XX.XX.xx</h4>
          </li>
          <li class="list-group-item">
            {{-- <div class="form-group">
              <a href="/touroperator/{{$viewtourpackages->id}}/edit" class="btn btn-warning"><i data-feather="edit">Edit</i></a>
              <a href="/touroperator/" class="btn btn-danger"><i data-feather="trash-2">Delete</i></a>
            </div> --}}
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
@endsection