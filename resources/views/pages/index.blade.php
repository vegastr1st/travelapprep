@extends('layouts.app')

@section('content')
    {{-- <h1>{{$title}}</h1>
    <p>This is Home page</p> --}}

    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner img-fluid">
        <div class="carousel-caption d-none d-md-block">
          
          <div class="row-fluid align-items-center">
            <div class="card text-center">
              <div class="card-body">
                <h1>Put Something</h1>          
                <form>
                  <div class="form-row">
                    <div class="col-4">
                      <input type="text" class="form-control" placeholder="Destination">
                    </div>
                    <div class="col-2">
                      <input type="date" class="form-control" placeholder="Check In">
                    </div>
                    <div class="col-2">
                      <input type="date" class="form-control" placeholder="Check Out">
                    </div>
                    <div class="col">
                      <input type="text" class="form-control" placeholder="Adults">
                    </div>
                    <div class="col">
                      <input type="text" class="form-control" placeholder="Child">
                    </div>
                    <div class="col">
                      <input type="text" class="form-control" placeholder="Infant">
                    </div>
                    <div class="col">
                      <a href="#" class="btn btn-primary">Search</a>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
          
        </div>
        <div class="carousel-item active">
          <img src="https://via.placeholder.com/800x250" class="d-block w-100" alt="...">
          <div class="carousel-caption d-none d-md-block">
          </div>
        </div>
        <div class="carousel-item">
          <img src="https://via.placeholder.com/800x250" class="d-block w-100" alt="...">
        </div>
        <div class="carousel-item">
          <img src="https://via.placeholder.com/800x250" class="d-block w-100" alt="...">
        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>

{{-- <br>
  <div class="container">
    <div class="card-deck">
      @if (count($testtourpackages) > 0)
        @foreach ($testtourpackages as $testtourpackage)
          <div class="card">
            <img src="https://via.placeholder.com/200x100" class="card-img-top" alt="...">
            <div class="card-body">
              <h3 class="card-title"><a href="/index/{{$testtourpackage->id}}">{{$testtourpackage->tp_name}}</a></h3>

              <p class="card-text">{{$testtourpackage->tp_description}}</p>
            </div>
            <div class="card-footer">
              <small class="text-muted">Created {{$testtourpackage->created_at}}</small>

            </div>
          </div>
        @endforeach

      @else
        <p>No Data Found</p>
      @endif
    </div>
  </div>

<br> --}}

<br>
  <div class="container">
    <div class="card-deck">
      @if (count($jointests) > 0)
        @foreach ($jointests as $jointest)
          <div class="card">
            <img src="https://via.placeholder.com/200x100" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">
                <div class="row">
                  <div class="col">
                    <ul class="list-group">P{{$jointest->tp_price}}</ul>
                    <ul class="list-group">* * * * *</ul>
                  </div>
                  <div class="col-6">
                    <div class="btn-group" role="group" aria-label="Basic example">
                      <button type="button" class="btn btn-secondary alert-light">Day {{$jointest->tp_day}}</button>
                      <button type="button" class="btn btn-secondary alert-dark">Night {{$jointest->tp_night}}</button>
                    </div>
                  </div>
                </div>
              </h5>
              <h5 class="card-title">{{$jointest->tp_name}}</a></h5>
              {{-- <h5 class="card-title">Card title</h5> --}}
              <p class="card-text">{{$jointest->tp_description}}</p>
            </div>
            <div class="card-footer">
              <div class="row">
                <div class="col"><small class="text-muted">Created {{$jointest->created_at}}</small></div>
                <div class="col-5"><a href="/travelagent/{{$jointest->id}}" class="btn btn-primary">View Tour Package</a></div>
              </div>
              {{-- <small class="text-muted">Last updated 3 mins ago</small> --}}
            </div>
          </div>
        @endforeach
        {{-- {{$testtourpackages->links()}} --}}
      @else
        <p>No Data Found</p>
      @endif
    </div>
  </div>
<br>

<div class="container">
  <div class="row">
    <div class="col">
      @if (count($jointests) > 0)
          @foreach ($jointests as $jointest)
          <div class="card mb-3" style="max-width: 540px;">
            <div class="row no-gutters">
              <div class="col-md-4">
                <img src="https://via.placeholder.com/200x200" class="card-img" alt="...">
              </div>
              <div class="col-md-8">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title">{{$jointest->tp_name}}</h5>
                    </div>
                    <div class="col-5">
                      <div class="col">
                        <ul class="list-group">P{{$jointest->tp_price}}</ul>
                        <ul class="list-group">* * * * *</ul>
                      </div>
                    </div>
                  </div>
                  
                  <p class="card-text">{{$jointest->tp_description}}</p>
                </div>
                <div class="card-footer">
                  <div class="row">
                    <div class="col"><small class="text-muted">Created {{$jointest->created_at}}</small></div>
                    <div class="col-4"><a href="#" class="btn btn-primary">Book</a></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          @endforeach
      @else
        <p>No Data Found</p>
      @endif
    </div>
  </div>
</div>
@endsection