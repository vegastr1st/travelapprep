@extends('layouts.app')

@section('content')
<body>
  <div class="container-fluid">
    <div class="row">
      <div class="col-3">
        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
          <img src="https://via.placeholder.com/300x300" alt="..." class="img-thumbnail" alt="Responsive image">
          <a class="nav-link active" id="v-pills-tourpackagelist-tab" data-toggle="pill" href="#v-pills-tourpackagelist" role="tab" aria-controls="v-pills-tourpackagelist" aria-selected="true">List of Tour Package</a>
          <a class="nav-link" id="v-pills-createtourpackage-tab" data-toggle="pill" href="#v-pills-createtourpackage" role="tab" aria-controls="v-pills-createtourpackage" aria-selected="false">Create Tour Package</a>
          <a class="nav-link" id="v-pills-requestctp-tab" data-toggle="pill" href="#v-pills-requestctp" role="tab" aria-controls="v-pills-requestctp" aria-selected="false">Request Customized Tour Package</a>
          <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">Settings</a>
        </div>
      </div>
      <div class="col">
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
              <p>{{$message}}</p>
            </div>
        @endif
        <div class="tab-content" id="v-pills-tabContent">
          {{-- List of Tour Package --}}
          <div class="tab-pane fade show active" id="v-pills-tourpackagelist" role="tabpanel" aria-labelledby="v-pills-tourpackagelist-tab">
            {{-- search tour package --}}
            <div class="input-group mb-3">
              <input type="text" class="form-control" placeholder="Search Tour Package" aria-label="Recipient's username" aria-describedby="button-addon2">
              <div class="input-group-append">
                <button class="btn btn-outline-success" type="button" id="button-addon2">Search</button>
              </div>
            </div>
            <table class="table">
              <thead class="thead-light">
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Tour Package</th>
                  <th scope="col">Category</th>
                  <th scope="col">Price</th>
                  <th scope="col">Stocks</th>
                  <th scope="col">Approval</th>
                  <th scope="col">Tools</th>
                </tr>
              </thead>
              <tbody>
                @if(count($tourpackages)>0)
                <ul>
                  @foreach ($tourpackages as $tourpackage)
                  <tr>
                    <td scope="col">{{$tourpackage->id}}</td>
                    <td scope="col">{{$tourpackage->tp_name}}</td>
                    <td scope="col">{{$tourpackage->tp_category}}</td>
                    <td scope="col">P {{$tourpackage->tp_price}}</td>
                    <td scope="col">[xx]</td>
                    <td><button type="button" class="btn btn-success"><i data-feather="check"></i> Validated</button></td>
                    <td>
                      <div class="btn-group" role="group" aria-label="Basic example">
                        <a href="/touroperator/{{$tourpackage->id}}" class="btn btn-primary"><i data-feather="eye"></i></a>
                        <form method="POST" action="{{url('touroperator', $tourpackage->id)}}">
                          <input type="hidden" name="_method" value="DELETE">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          <button class="btn btn-danger"><i data-feather="trash-2"></i></button>
                        </form>
                      </div>
                    </td>
                  </tr>  
                  <script>
                    feather.replace()
                  </script>
                  @endforeach
                </ul>
                @endif
              </tbody>
            </table>
          </div>
          {{-- Create Tour Package Content--}}
          <div class="tab-pane fade" id="v-pills-createtourpackage" role="tabpanel" aria-labelledby="v-pills-createtourpackage-tab">
            <form class="was-validated" method='POST' action="{{url('touroperator')}}">
              <div class="row">
                {{-- Center of Creating Tour Package --}}
                <div class="col">
                  <div class="card w-100">
                    <div class="card card-body">
                      {{-- <div class="container"> --}}
                        <div class="row">
                          {{-- upload image --}}
                          <div class="col">
                            <img src="https://via.placeholder.com/400x250" class="rounded float-left" alt="...">
                          </div>
                          {{-- 1st Data Information Input --}}
                          <div class="col">
                            <div class="float-right">
                              <h3>Tour Package</h3>
                            </div>
                            <div class="form-group">
                              <input type="text" name="tp_name" value="{{ old('value')}}" class="form-control" id="formGroupTourPackageInput" placeholder="Package Name" required>
                              {{-- <div>{{ $errors->first('tp_name') }}</div> --}}
                            </div>
                            <div class="form-group">
                              <label for="formGroupTourPackageInput2">Category</label>
                              <input type="text" name="tp_category" value="" class="form-control" id="formGroupTourPackageInput2" placeholder="Category" required>
                            </div>
                            <div class="form-group">
                              <label for="formGroupTourPackageInput3">Price</label>
                              <input type="text" name="tp_price" value="" class="form-control" id="formGroupTourPackageInput3" placeholder="Price" required>
                            </div> 
                          </div>
                          {{-- 1st Data Information Input Extention --}}
                          <div class="container w-100">
                            <div class="form-row">
                              <div class="col-md-2">
                                <label for="TourPackagePaxInput">Start Date</label>
                                <input type="date" name="tp_start" class="form-control" id="TourPackagePaxInput" placeholder="Start Date" required>
                              </div>
                              <div class="col-md-2">
                                <label for="TourPackagePaxInput2">End Date</label>
                                <input type="date" name="tp_end" class="form-control" id="TourPackagePaxInput2" placeholder="End Date" required>
                              </div>
                              <div class="col-md-4">
                                <label for="TourPackagePaxInput3">Departure</label>
                                <input type="text" name="tp_departure" class="form-control" id="TourPackagePaxInput3" placeholder="Departure" required>
                              </div>
                              <div class="col-md-4">
                                <label for="TourPackagePaxInput4">Destination</label>
                                <input type="text" name="tp_destination" class="form-control" id="TourPackagePaxInput4" placeholder="Destination">
                              </div>
                            </div>
                          </div>
                          {{-- <div class="w-100"><br></div> --}}
                          <div class="col-md-6">
                            <label for="">Description</label>
                            <textarea name="tp_description" value="" class="form-control" id="textarea1" row="7" placeholder="Description"></textarea>
                          </div>
                          {{-- 2nd Data Information Input --}}
                          <div class="col-md-6">
                            <table class="table">
                              <div class="form-group">
                                <tbody>
                                  <tr>
                                    <td>Adult</td>
                                    <td><input type="checkbox" class="form-check-input" name="tp_adult" id="formGroupTourPackagePaxInput1" value="1" data-toggle="toggle" data-on="Enable" data-off="Disable" data-onstyle="success" data-offstyle="danger" data-width="100" required></td>
                                  </tr>
                                  <tr>
                                    <td>Child</td>
                                    <td><input type="checkbox" class="form-check-input" name="tp_child" id="formGroupTourPackagePaxInput2" value="1" data-toggle="toggle" data-on="Enable" data-off="Disable" data-onstyle="success" data-offstyle="danger" data-width="100"></td>
                                  </tr>
                                  <tr>
                                    <td>Infant</td>
                                    <td><input type="checkbox" class="form-check-input" name="tp_infant" id="formGroupTourPackagePaxInput3" value="1" data-toggle="toggle" data-on="Enable" data-off="Disable" data-onstyle="success" data-offstyle="danger" data-width="100"></td>
                                  </tr>
                                </tbody>
                              </div>
                            </table>
                          </div>
                        </div>
                      {{-- </div> --}}
  
                      {{-- center tab --}}
                      <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                          <a class="nav-link active" id="location-tab" data-toggle="tab" href="#location" role="tab" aria-controls="location" aria-selected="true">Location</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link disabled" id="inclusion-tab" data-toggle="tab" href="#inclusion" role="tab" aria-controls="inclusion" aria-selected="false">Inclusion</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link disabled" id="exclution-tab" data-toggle="tab" href="#exclution" role="tab" aria-controls="exclution" aria-selected="false">Exclusion</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" id="transport-tab" data-toggle="tab" href="#transport" role="tab" aria-controls="transport" aria-selected="false">Transport</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" id="paymentPolicy-tab" data-toggle="tab" href="#paymentPolicy" role="tab" aria-controls="paymentPolicy" aria-selected="false">Payment and Policy</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Contact</a>
                        </li>
                      </ul>
                      {{-- center content --}}
                      {{-- 3rd, 4th, 5th Data Information Input --}}
                      <div class="tab-content" id="myTabContent">
                        {{-- Location --}}
                        <div class="tab-pane fade show active" id="location" role="tabpanel" aria-labelledby="location-tab">
                          <div class="w-100"><br></div>                       
                            <div class="row">
                              <div class="col">
                                <ul>
                                  <div class="form-group row">
                                    <label for="" class="col-sm-1.5 col-form-label">01.</label>
                                    <div class="col-sm-11">
                                      <input type="text" name="tp_loc1" value="{{ old('value')}}" class="form-control" id="formGroupTourPackageLocationInput1" placeholder="Location">
                                    </div>
                                  </div>
                                </ul>
                                <ul>
                                  <div class="form-group row">
                                    <label for="" class="col-sm-1.5 col-form-label">02.</label>
                                    <div class="col-sm-11">
                                      <input type="text" name="tp_loc2" class="form-control" id="formGroupTourPackageLocationInput2" placeholder="Location">
                                    </div>
                                  </div>
                                </ul>
                                <ul>
                                  <div class="form-group row">
                                    <label for="" class="col-sm-1.5 col-form-label">03.</label>
                                    <div class="col-sm-11">
                                      <input type="text" name="tp_loc3" class="form-control" id="formGroupTourPackageLocationInput3" placeholder="Location">
                                    </div>
                                  </div>
                                </ul>
                                <ul>
                                  <div class="form-group row">
                                    <label for="" class="col-sm-1.5 col-form-label">04.</label>
                                    <div class="col-sm-11">
                                      <input type="text" name="tp_loc4" class="form-control" id="formGroupTourPackageLocationInput4" placeholder="Location">
                                    </div>
                                  </div>
                                </ul>
                                <ul>
                                  <div class="form-group row">
                                    <label for="" class="col-sm-1.5 col-form-label">05.</label>
                                    <div class="col-sm-11">
                                      <input type="text" name="tp_loc5" class="form-control" id="formGroupTourPackageLocationInput5" placeholder="Location">
                                    </div>
                                  </div>
                                </ul>
                              </div>
                              <div class="col">
                                <ul>
                                  <div class="form-group row">
                                    <label for="" class="col-sm-1.5 col-form-label">06.</label>
                                    <div class="col-sm-11">
                                      <input type="text" name="tp_loc6" class="form-control" id="formGroupTourPackageLocationInput6" placeholder="Location">
                                    </div>
                                  </div>
                                </ul>
                                <ul>
                                  <div class="form-group row">
                                    <label for="" class="col-sm-1.5 col-form-label">07.</label>
                                    <div class="col-sm-11">
                                      <input type="text" name="tp_loc7" class="form-control" id="formGroupTourPackageLocationInput7" placeholder="Location">
                                    </div>
                                  </div>
                                </ul>
                                <ul>
                                  <div class="form-group row">
                                    <label for="" class="col-sm-1.5 col-form-label">08.</label>
                                    <div class="col-sm-11">
                                      <input type="text" name="tp_loc8" class="form-control" id="formGroupTourPackageLocationInput8" placeholder="Location">
                                    </div>
                                  </div>
                                </ul>
                                <ul>
                                  <div class="form-group row">
                                    <label for="" class="col-sm-1.5 col-form-label">09.</label>
                                    <div class="col-sm-11">
                                      <input type="text" name="tp_loc9" class="form-control" id="formGroupTourPackageLocationInput9" placeholder="Location">
                                    </div>
                                  </div>
                                </ul>
                                <ul>
                                  <div class="form-group row">
                                    <label for="" class="col-sm-1.5 col-form-label">10.</label>
                                    <div class="col-sm-11">
                                      <input type="text" name="tp_loc10" class="form-control" id="formGroupTourPackageLocationInput10" placeholder="Location">
                                    </div>
                                  </div>
                                </ul>
                              </div>
                            </div>
                        </div>
                        {{-- Inclusion --}}
                        <div class="tab-pane fade" id="inclusion" role="tabpanel" aria-labelledby="inclusion-tab">
                          <div class="w-100"><br></div>
                          <div class="container row">
                            <div class="col">
                              <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="" value="1" id="inclusionCheck1" data-toggle="toggle" data-on="Enable" data-off="Disable">
                                <label class="form-check-label" for="inclusionCheck1">
                                  Accommodation
                                </label>
                              </div>
                            </div>
                            <div class="col">
                              <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="" value="1" id="inclusionCheck2" data-toggle="toggle" data-on="Enable" data-off="Disable">
                                <label class="form-check-label" for="inclusionCheck2">
                                  Full Board Meals (Breakfast, Lunch, Dinner)
                                </label>
                              </div>
                            </div>
                            <div class="col">
                              <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="" value="1" id="inclusionCheck3" data-toggle="toggle" data-on="Enable" data-off="Disable">
                                <label class="form-check-label" for="inclusionCheck3">
                                  Airport Transfers
                                </label>
                              </div>
                            </div>
                          </div>
                          <div class="container row">
                            <div class="col">
                              <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="" value="1" id="inclusionCheck4" data-toggle="toggle" data-on="Enable" data-off="Disable">
                                <label class="form-check-label" for="inclusionCheck4">
                                  Land Transport
                                </label>
                              </div>
                            </div>
                            <div class="col">
                              <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="" value="1" id="inclusionCheck5" data-toggle="toggle" data-on="Enable" data-off="Disable">
                                <label class="form-check-label" for="inclusionCheck5">
                                  Boat Transfers (For Sabtang Island Tour)
                                </label>
                              </div>
                            </div>
                            <div class="col">
                              <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="" value="1" id="inclusionCheck6" data-toggle="toggle" data-on="Enable" data-off="Disable">
                                <label class="form-check-label" for="inclusionCheck6">
                                  Tour Guide (Provincial and DOT Accredited Guides)
                                </label>
                              </div>
                            </div>
                          </div>
                          <div class="container row">
                            <div class="col">
                              <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="" value="1" id="inclusionCheck7" data-toggle="toggle" data-on="Enable" data-off="Disable">
                                <label class="form-check-label" for="inclusionCheck7">
                                  Municipal Fees
                                </label>
                              </div>
                            </div>
                            <div class="col">
                              <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="" value="1" id="inclusionCheck8" data-toggle="toggle" data-on="Enable" data-off="Disable">
                                <label class="form-check-label" for="inclusionCheck8">
                                  Tax Included
                                </label>
                              </div>
                            </div>
                            <div class="col">
                              <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="" value="1" id="inclusionCheck9" data-toggle="toggle" data-on="Enable" data-off="Disable">
                                <label class="form-check-label" for="inclusionCheck9">
                                  Eco-Tourism Fee
                                </label>
                              </div>
                            </div>
                          </div>
                          <div class="container row">
                            <div class="col">
                              <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="" value="1" id="inclusionCheck10" data-toggle="toggle" data-on="Enable" data-off="Disable">
                                <label class="form-check-label" for="inclusionCheck10">
                                  Lunch and dinner meals during free days are on pax account
                                </label>
                              </div>
                            </div>
                            <div class="col">
                              {{-- <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="" value="1" id="inclusionCheck11" data-toggle="toggle" data-on="Enable" data-off="Disable">
                                <label class="form-check-label" for="inclusionCheck11">
                                  Lunch and dinner meals during free days are on pax account
                                </label>
                              </div> --}}
                            </div>
                            <div class="col">
                              {{-- <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="inclusionCheck12">
                                <label class="form-check-label" for="inclusionCheck12">
                                  Lunch and dinner meals during free days are on pax account
                                </label>
                              </div> --}}
                            </div>
                          </div>
        
                        </div>
                        {{-- Exclusion --}}
                        <div class="tab-pane fade" id="exclution" role="tabpanel" aria-labelledby="exclution-tab">
                          <div class="w-100"><br></div>
                          <div class="container row">
                            <div class="col">
                              <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="" value="1" id="exclutionCheck1" data-toggle="toggle" data-on="Enable" data-off="Disable">
                                <label class="form-check-label" for="exclutionCheck1">
                                  Airfare/ Train Fare
                                </label>
                              </div>
                            </div>
                            <div class="col">
                              <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="" value="1" id="exclutionCheck2" data-toggle="toggle" data-on="Enable" data-off="Disable">
                                <label class="form-check-label" for="exclutionCheck2">
                                  Personal Expenses like, Laundry shopping, Tips
                                </label>
                              </div>
                            </div>
                            <div class="col">
                              <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="" value="1" id="exclutionCheck3" data-toggle="toggle" data-on="Enable" data-off="Disable">
                                <label class="form-check-label" for="exclutionCheck3">
                                  Monument Entry Fee / Camera Fees
                                </label>
                              </div>
                            </div>
                          </div>
                          <div class="container row">
                            <div class="col">
                              <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="" value="1" id="exclutionCheck4" data-toggle="toggle" data-on="Enable" data-off="Disable">
                                <label class="form-check-label" for="exclutionCheck4">
                                  Adventure Activities - River Rafting, Skiing, Paragliding
                                </label>
                              </div>
                            </div>
                            <div class="col">
                              <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="" value="1" id="exclutionCheck5" data-toggle="toggle" data-on="Enable" data-off="Disable">
                                <label class="form-check-label" for="exclutionCheck5">
                                  Any additional services: F&B, Travel Insurance, Heater
                                </label>
                              </div>
                            </div>
                            <div class="col">
                              <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="" value="1" id="exclutionCheck6" data-toggle="toggle" data-on="Enable" data-off="Disable">
                                <label class="form-check-label" for="exclutionCheck6">
                                  Requirement, Extra Sightseeing, etc
                                </label>
                              </div>
                            </div>
                          </div>
                          <div class="container row">
                            <div class="col">
                              <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="" value="1" id="exclutionCheck7" data-toggle="toggle" data-on="Enable" data-off="Disable">
                                <label class="form-check-label" for="exclutionCheck7">
                                  Volvo Luggage charges (If applied by transporter)
                                </label>
                              </div>
                            </div>
                            <div class="col">
                              <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="" value="1" id="exclutionCheck8" data-toggle="toggle" data-on="Enable" data-off="Disable">
                                <label class="form-check-label" for="exclutionCheck8">
                                  5% GST Extra.
                                </label>
                              </div>
                            </div>
                            <div class="col">
                              {{-- <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="" value="1" id="exclutionCheck9" data-toggle="toggle" data-on="Enable" data-off="Disable">
                                <label class="form-check-label" for="exclutionCheck9">
                                  5% GST Extra.
                                </label>
                              </div> --}}
                            </div>
                          </div> 
                        </div>
                        {{-- Transport --}}
                        <div class="tab-pane fade" id="transport" role="tabpanel" aria-labelledby="transport-tab">
                          <div class="w-100"><br></div>
                          <div class="container row">
                            <div class="col">
                              <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="tp_cab" value="1" id="transportCheck1" data-toggle="toggle" data-on="Enable" data-off="Disable">
                                <label class="form-check-label" for="transportCheck1">
                                  Cab
                                </label>
                              </div>
                            </div>
                            <div class="col">
                              <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="tp_car" value="1" id="transportCheck2" data-toggle="toggle" data-on="Enable" data-off="Disable">
                                <label class="form-check-label" for="transportCheck2">
                                  Car
                                </label>
                              </div>
                            </div>
                            <div class="col">
                              <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="tp_bus" value="1" id="transportCheck3" data-toggle="toggle" data-on="Enable" data-off="Disable">
                                <label class="form-check-label" for="transportCheck3">
                                  Bus
                                </label>
                              </div>
                            </div>
                          </div>
                          <div class="container row">
                            <div class="col">
                              <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="tp_van" value="1" id="transportCheck4" data-toggle="toggle" data-on="Enable" data-off="Disable">
                                <label class="form-check-label" for="transportCheck4">
                                  Van
                                </label>
                              </div>
                            </div>
                            <div class="col">
                              <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="tp_coasterbus" value="1" id="transportCheck5" data-toggle="toggle" data-on="Enable" data-off="Disable">
                                <label class="form-check-label" for="transportCheck5">
                                  Coaster Bus
                                </label>
                              </div>
                            </div>
                            <div class="col">
                              <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="tp_flight" value="1" id="transportCheck6" data-toggle="toggle" data-on="Enable" data-off="Disable">
                                <label class="form-check-label" for="transportCheck6">
                                  Flight
                                </label>
                              </div>
                            </div>
                          </div>
                          <div class="container row">
                            <div class="col">
                              <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="tp_cruise" value="1" id="transportCheck7" data-toggle="toggle" data-on="Enable" data-off="Disable">
                                <label class="form-check-label" for="transportCheck7">
                                  Cruise
                                </label>
                              </div>
                            </div>
                            <div class="col">
                              {{-- <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="tp_cab" value="1" id="transportCheck1" data-toggle="toggle" data-on="Enable" data-off="Disable">
                                <label class="form-check-label" for="transportCheck1">
                                  5% GST Extra.
                                </label>
                              </div> --}}
                            </div>
                            <div class="col">
                              {{-- <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="tp_cab" value="1" id="transportCheck1" data-toggle="toggle" data-on="Enable" data-off="Disable">
                                <label class="form-check-label" for="transportCheck1">
                                  5% GST Extra.
                                </label>
                              </div> --}}
                            </div>
                          </div> 
                        </div>
                        {{-- Payment and Policy --}}
                        <div class="tab-pane fade" id="paymentPolicy" role="tabpanel" aria-labelledby="paymentPolicy-tab">
                          <div class="w-100"><br></div>
                          <div class="container row">
                            <div class="col">
                              Payment Option: 
                            </div>
                            {{-- bank --}}
                            <div class="col">
                              <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="" id="inlineCheckbox1" value="AnyBank" data-toggle="toggle" data-on="Enable" data-off="Disable">
                                <label class="form-check-label" for="inlineCheckbox1">Any Bank??</label>
                              </div>
                            </div>
                            {{-- Paypal --}}
                            <div class="col">
                              <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="" id="inlineCheckbox2" value="Paypal" data-toggle="toggle" data-on="Enable" data-off="Disable">
                                <label class="form-check-label" for="inlineCheckbox2">Paypal</label>
                              </div>
                            </div>
                            {{-- Gcash --}}
                            <div class="col">
                              <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="" id="inlineCheckbox3" value="GCash" data-toggle="toggle" data-on="Enable" data-off="Disable">
                                <label class="form-check-label" for="inlineCheckbox3">GCash</label>
                              </div>
                            </div>
                          </div>
                          <div class="w-100"><br></div>
                          <div class="container row">
                            <div class="col">
                              <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="1" id="defaultCheck1" data-toggle="toggle" data-on="Enable" data-off="Disable">
                                <label class="form-check-label" for="defaultCheck1">
                                  Package is non-refundable
                                </label>
                              </div>
                            </div>
                            <div class="col">
                              <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="1" id="defaultCheck2" data-toggle="toggle" data-on="Enable" data-off="Disable">
                                <label class="form-check-label" for="defaultCheck2">
                                  Package re-bookable with respective charges
                                </label>
                              </div>
                            </div>
                            <div class="col">
                              <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="1" id="defaultCheck3" data-toggle="toggle" data-on="Enable" data-off="Disable">
                                <label class="form-check-label" for="defaultCheck3">
                                  Package is transferable
                                </label>
                              </div>
                            </div>
                          </div>
                          <div class="w-100"><br></div>
                          <div class="container row">
                            <form>
                              <div class="form-row">
                                <div class="form-group col-md">
                                  <label for="inputEmail4">Policy Type:</label>
                                  <input type="text" name="tp_policy_type" class="form-control" id="formGroupTourPackagePolicyInput1" placeholder="Policy Type">
                                </div>
                                <div class="form-group col-md-4">
                                  <label for="inputCoverage">Coverage</label>
                                  <select name="tp_coverage" id="inputCoverage" class="form-control">
                                    <option value="Cancelation">Cancelation</option>
                                    <option value="Medical">Medical</option>
                                    <option value="Lost Baggage">Lost Baggage</option>
                                    <option value="Emergency Evacuation">Emergency Evacuation</option>
                                    <option value="Legal Costs">Legal Costs</option>
                                    <option value="Protection of property">Protection of property</option>
                                  </select>
                                </div>
                              </div>
                              <div class="form-row">
                                <div class="form-group col-md-4">
                                  <label for="inputEmail4">Policy Price</label>
                                  <input type="text" name="tp_policy_price" class="form-control" id="formGroupTourPackagePolicyInput3" placeholder="Policy Price">
                                </div>
                                <div class="form-group col-md-4">
                                  <label for="inputEmail4">Deductible</label>
                                  <input type="text" name="tp_deductible" class="form-control" id="formGroupTourPackagePolicyInput4" placeholder="Deductible">
                                </div>
                                <div class="form-group col-md-4">
                                  <label for="inputEmail4">Deductible</label>
                                  <input type="date" name="tp_issued_date" class="form-control" id="formGroupTourPackagePolicyInput5" placeholder="Date">
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                        {{-- Contact --}}
                        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                          <div class="w-100"><br></div>
                          <form>
                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <label for="inputEmail4">Tour Operator Email</label>
                                <input type="email" class="form-control" id="inputEmail4">
                              </div>
                              <div class="form-group col-md-6">
                                <label for="inputPassword4">Tour Operator Phone No.</label>
                                <input type="password" class="form-control" id="inputPassword4">
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="inputAddress">Address</label>
                              <input type="text" class="form-control" id="inputAddress" placeholder="1234 Main St">
                            </div>
                            <div class="form-group">
                              <label for="inputAddress2">Address 2</label>
                              <input type="text" class="form-control" id="inputAddress2" placeholder="Apartment, studio, or floor">
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {{-- Right SideBar Input Data and Details --}}
                <div class="col-3">
                  <div class="row">
                    <ul class="list-group">
                      <li class="list-group-item">
                        <div class="row">
                          <div class="col-3">Rating</div>
                          <div class="col">
                            <input type="text" name="tp_rating" class="form-control" id="formGroupTourPackageSettingInput1" placeholder="(1-5)">
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-3">Day</div>
                          <div class="col">
                            <input type="text" name="tp_day" class="form-control" id="formGroupTourPackageSettingInput2" placeholder="Day">
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-3">Night</div>
                          <div class="col">
                            <input type="text" name="tp_night" class="form-control" id="formGroupTourPackageSettingInput3" placeholder="Night">
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-3">VAT</div>
                          <div class="col">
                            <input type="text" name="tp_vat" class="form-control" id="formGroupTourPackageSettingInput4" placeholder="Vat">
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-3">Tax</div>
                          <div class="col">
                            <input type="text" name="tp_tax" class="form-control" id="formGroupTourPackageSettingInput5" placeholder="Tax">
                          </div>
                        </div>
                      </li>
                      <li class="list-group-item">
                        <div class="row">
                          <div class="col">Location</div>
                          <div class="col">Check</div>
                        </div>
                        <div class="row">
                          <div class="col">Inclusion</div>
                          <div class="col">Check</div>
                        </div>
                        <div class="row">
                          <div class="col">Exclusion</div>
                          <div class="col">Check</div>
                        </div>
                        <div class="row">
                          <div class="col">Payment And Policy</div>
                          <div class="col">Check</div>
                        </div>
                        <div class="row">
                          <div class="col">Contact</div>
                          <div class="col">Check</div>
                        </div>
                      </li>
                      <li class="list-group-item">
                        <div class="row">
                          <h4>Payment Detials</h4>
                        </div>
                        <div class="row">
                          <div class="col">Total Stay (No.)</div>
                          <div class="col">Check</div>
                        </div>
                        <div class="row">
                          <div class="col">VAT</div>
                          <div class="col">Check</div>
                        </div>
                        <div class="row">
                          <div class="col">Tax</div>
                          <div class="col">Check</div>
                        </div>
                        <div class="row">
                          <div class="col">Inclusion</div>
                          <div class="col">Check</div>
                        </div>
                        <div class="row">
                          <div class="col">Exclusion</div>
                          <div class="col">Check</div>
                        </div>
                      </li>
                      <li class="list-group-item">
                        <h4>P XX.XX.xx</h4>
                      </li>
                      <li class="list-group-item">
                        <div class="form-group">
                          <input type="submit" class="btn btn-primary">
                        </div>
                        @csrf
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </form>
          </div>
          {{-- Request Customized Tour Package Content --}}
          <div class="tab-pane fade" id="v-pills-requestctp" role="tabpanel" aria-labelledby="v-pills-requestctp-tab">
            {{-- search tour package --}}
            <div class="input-group mb-3">
              <input type="text" class="form-control" placeholder="Search Tour Package" aria-label="Recipient's username" aria-describedby="button-addon2">
              <div class="input-group-append">
                <button class="btn btn-outline-success" type="button" id="button-addon2">Search</button>
              </div>
            </div>
            <table class="table">
              <thead class="thead-light">
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Tour Package</th>
                  <th scope="col">Category</th>
                  <th scope="col">Duration</th>
                  <th scope="col">Budget</th>
                  <th scope="col">Tools</th>
                </tr>
              </thead>
              <tbody>
                @if(count($tourpackages) > 0)
                <ul>
                    @foreach($tourpackages as $tourpackages)
                    <tr>
                        {{-- <td scope="col">{{$tourpackages->id}}</td>
                        <td scope="col">{{$tourpackages->tp_name}}</td>
                        <td scope="col">{{$tourpackages->tp_category}}</td>
                        <td scope="col">[xx]</td>
                        <td scope="col">Pxx.xx</td>
                        <td><button type="button" class="btn btn-warning"><i data-feather="edit"></i></button> <button type="button" class="btn btn-danger"><i data-feather="trash-2"></i></button></td> --}}
                      </tr>  
                      <script>
                        feather.replace()
                      </script>
                    @endforeach
                </ul>
                @endif
              </tbody>
            </table>
          </div>
          {{-- Setting Content --}}
          <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt dolor distinctio nostrum numquam ipsam at tempore earum similique excepturi cumque aliquam voluptatibus voluptatem ipsum, ab omnis sed minus adipisci incidunt!</div>
        </div>
      </div>
    </div>
  </div>
</body>
@endsection