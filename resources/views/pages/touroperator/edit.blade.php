@extends('layouts.app')

@section('content')
<h1>Tour Operator/{{$edittourpackages->id}}/Edit</h1>
<div class="container">
{{-- <div class="tab-panel fade" id="v-pills-createtourpackage" role="tabpanel" aria-labelledby="v-pills-createtourpackage-tab"> --}}
  <form class="was-validated" method='POST' action="{{ url('touroperator', $edittourpackages->id) }}">
    {{-- <input type="hidden" name="_method" value="PATCH"> --}}
    {{-- @csrf --}}
    {{-- @method('PUT') --}}
    <div class="row">
      {{-- Center of Creating Tour Package --}}
      <div class="col">
        <div class="card w-100">
          <div class="card card-body">
            {{-- <div class="container"> --}}
              <div class="row">
                {{-- upload image --}}
                <div class="col">
                  <img src="https://via.placeholder.com/400x250" class="rounded float-left" alt="...">
                </div>
                {{-- 1st Data Information Input --}}
                <div class="col">
                  <div class="float-right">
                    <h3>Tour Package</h3>
                  </div>
                  <div class="form-group">
                    <input type="text" name="tp_name" value="{{$edittourpackages->tp_name}}" class="form-control" id="formGroupTourPackageInput" placeholder="Package Name" required>
                    {{-- <div>{{ $errors->first('tp_name') }}</div> --}}
                  </div>
                  <div class="form-group">
                    <label for="formGroupTourPackageInput2">Category</label>
                    <input type="text" name="tp_category" value="{{$edittourpackages->tp_category}}" class="form-control" id="formGroupTourPackageInput2" placeholder="Category" required>
                  </div>
                  <div class="form-group">
                    <label for="formGroupTourPackageInput3">Price</label>
                    <input type="text" name="tp_price" value="{{$edittourpackages->tp_price}}" class="form-control" id="formGroupTourPackageInput3" placeholder="Price" required>
                  </div> 
                </div>
                {{-- 1st Data Information Input Extention --}}
                <div class="container w-100">
                  <div class="form-row">
                    <div class="col-md-2">
                      <label for="TourPackagePaxInput">Start Date</label>
                      <input type="date" name="tp_start" value="{{$edittourpackages->tp_start}}" class="form-control" id="TourPackagePaxInput" placeholder="Start Date" required>
                    </div>
                    <div class="col-md-2">
                      <label for="TourPackagePaxInput2">End Date</label>
                      <input type="date" name="tp_end" value="{{$edittourpackages->tp_end}}" class="form-control" id="TourPackagePaxInput2" placeholder="End Date" required>
                    </div>
                    <div class="col-md-4">
                      <label for="TourPackagePaxInput3">Departure</label>
                      <input type="text" name="tp_departure" value="{{$edittourpackages->tp_departure}}" class="form-control" id="TourPackagePaxInput3" placeholder="Departure" required>
                    </div>
                    <div class="col-md-4">
                      <label for="TourPackagePaxInput4">Destination</label>
                      <input type="text" name="tp_destination" value="{{$edittourpackages->tp_destination}}" class="form-control" id="TourPackagePaxInput4" placeholder="Destination">
                    </div>
                  </div>
                </div>
                {{-- <div class="w-100"><br></div> --}}
                <div class="col-md-6">
                  <label for="">Description</label>
                  <textarea name="tp_description" value="{{$edittourpackages->tp_description}}" class="form-control" id="textarea1" row="7" placeholder="Description"></textarea>
                </div>
                {{-- 2nd Data Information Input --}}
                <div class="col-md-6">
                  <table class="table">
                    <div class="form-group">
                      <tbody>
                        <tr>
                          <td>Adult</td>
                          <td>{{$tppass->tp_adult}}<input type="checkbox" class="form-check-input" name="tp_adult" id="formGroupTourPackagePaxInput1" value="1" data-toggle="toggle" data-on="Enable" data-off="Disable" data-onstyle="success" data-offstyle="danger" data-width="100" required></td>
                        </tr>
                        <tr>
                          <td>Child</td>
                          <td>{{$tppass->tp_child}}<input type="checkbox" class="form-check-input" name="tp_child" id="formGroupTourPackagePaxInput2" value="1" data-toggle="toggle" data-on="Enable" data-off="Disable" data-onstyle="success" data-offstyle="danger" data-width="100"></td>
                        </tr>
                        <tr>
                          <td>Infant</td>
                          <td>{{$tppass->tp_infant}}<input type="checkbox" class="form-check-input" name="tp_infant" id="formGroupTourPackagePaxInput3" value="1" data-toggle="toggle" data-on="Enable" data-off="Disable" data-onstyle="success" data-offstyle="danger" data-width="100"></td>
                        </tr>
                      </tbody>
                    </div>
                  </table>
                </div>
              </div>
            {{-- </div> --}}
          </div>
        </div>
      </div>
      {{-- Right SideBar Input Data and Details --}}
      <div class="col-3">
        <div class="row">
          <ul class="list-group">
            <li class="list-group-item">
              <div class="row">
                <div class="col-3">Rating</div>
                <div class="col">
                  <input type="text" name="tp_rating" class="form-control" id="formGroupTourPackageSettingInput1" placeholder="(1-5)">
                </div>
              </div>
              <div class="row">
                <div class="col-3">Day</div>
                <div class="col">
                  <input type="text" name="tp_day" class="form-control" id="formGroupTourPackageSettingInput2" placeholder="Day">
                </div>
              </div>
              <div class="row">
                <div class="col-3">Night</div>
                <div class="col">
                  <input type="text" name="tp_night" class="form-control" id="formGroupTourPackageSettingInput3" placeholder="Night">
                </div>
              </div>
              <div class="row">
                <div class="col-3">VAT</div>
                <div class="col">
                  <input type="text" name="tp_vat" class="form-control" id="formGroupTourPackageSettingInput4" placeholder="Vat">
                </div>
              </div>
              <div class="row">
                <div class="col-3">Tax</div>
                <div class="col">
                  <input type="text" name="tp_tax" class="form-control" id="formGroupTourPackageSettingInput5" placeholder="Tax">
                </div>
              </div>
            </li>
            <li class="list-group-item">
              <div class="row">
                <div class="col">Location</div>
                <div class="col">Check</div>
              </div>
              <div class="row">
                <div class="col">Inclusion</div>
                <div class="col">Check</div>
              </div>
              <div class="row">
                <div class="col">Exclusion</div>
                <div class="col">Check</div>
              </div>
              <div class="row">
                <div class="col">Payment And Policy</div>
                <div class="col">Check</div>
              </div>
              <div class="row">
                <div class="col">Contact</div>
                <div class="col">Check</div>
              </div>
            </li>
            <li class="list-group-item">
              <div class="row">
                <h4>Payment Detials</h4>
              </div>
              <div class="row">
                <div class="col">Total Stay (No.)</div>
                <div class="col">Check</div>
              </div>
              <div class="row">
                <div class="col">VAT</div>
                <div class="col">Check</div>
              </div>
              <div class="row">
                <div class="col">Tax</div>
                <div class="col">Check</div>
              </div>
              <div class="row">
                <div class="col">Inclusion</div>
                <div class="col">Check</div>
              </div>
              <div class="row">
                <div class="col">Exclusion</div>
                <div class="col">Check</div>
              </div>
            </li>
            <li class="list-group-item">
              <h4>P XX.XX.xx</h4>
            </li>
            <li class="list-group-item">
              <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Update">
              </div>
              @csrf
            </li>
          </ul>
        </div>
      </div>
    </div>
  </form>
</div>

@endsection