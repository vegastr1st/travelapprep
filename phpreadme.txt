Git global setup
git config --global user.name "Tristan Vegas"
git config --global user.email "vegastristan1@gmail.com

Create a new repository
git clone https://gitlab.com/vegastr1st/travelapprep.git
cd travelapprep
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Push an existing folder
cd existing_folder
git init
git remote add origin https://gitlab.com/vegastr1st/travelapprep.git
git add .
git commit -m "Initial commit"
git push -u origin master

Push an existing Git repository
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/vegastr1st/travelapprep.git
git push -u origin --all
git push -u origin --tags

php artisan make:model "table"
php artisan migrate //to save table to database
php artisan migrate:rollback //back to 0 the database

insert data into Mysql

https://prium.github.io/falcon/v2.3.2/default/index.html


{{ $error->first('name') }}
value="{{ old('value')}}"


