<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTourPackageSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_package_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tp_id')->unsigned();
            $table->tinyInteger('tp_rating'); //rating of the tour package
            $table->tinyInteger('tp_day'); //days of the tour package
            $table->tinyInteger('tp_night'); //night of the tour package
            $table->decimal('tp_vat'); //set vat of the tour package
            $table->decimal('tp_tax'); //set tax of the tour package
            $table->foreign('tp_id')->references('id')->on('tour_packages');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_package_settings');
    }
}
