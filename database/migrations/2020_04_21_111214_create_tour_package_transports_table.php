<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTourPackageTransportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_package_transports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tp_id')->unsigned();
            $table->boolean('tp_cab')->default(1)->nullable();; //selected type of transport
            $table->boolean('tp_car')->default(1)->nullable();; //selected type of transport
            $table->boolean('tp_bus')->default(1)->nullable();; //selected type of transport
            $table->boolean('tp_van')->default(1)->nullable();; //selected type of transport
            $table->boolean('tp_coasterbus')->default(1)->nullable();; //selected type of transport
            $table->boolean('tp_flight')->default(1)->nullable();; //selected type of transport
            $table->boolean('tp_cruise')->default(1)->nullable();; //selected type of transport
            $table->foreign('tp_id')->references('id')->on('tour_packages');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_package_transports');
    }
}
