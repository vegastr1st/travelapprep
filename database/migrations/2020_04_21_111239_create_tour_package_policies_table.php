<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTourPackagePoliciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_package_policies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tp_id')->unsigned();
            $table->string('tp_policy_type'); //
            $table->string('tp_coverage'); //cancelation, medical, lost baggage, emergency evacuation, Legal costs (for unexpected incidents you didn’t cause), and Protection of property (gear and possessions) and more.
            $table->decimal('tp_policy_price'); //price of the policy price
            $table->decimal('tp_deductible'); // price of the first need to pay to deductible of policy
            $table->datetime('tp_issued_date'); //date of the issue date
            $table->foreign('tp_id')->references('id')->on('tour_packages');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_package_policies');
    }
}
