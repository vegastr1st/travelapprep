<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTourPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_packages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tp_name'); //varchar(255)
            $table->text('tp_description');
            $table->datetime('tp_start'); //date start of the tour package
            $table->datetime('tp_end'); //date end of the tour package
            $table->string('tp_departure'); //departure area of the current tour
            $table->string('tp_destination')->nullable();; //destination of the tour
            $table->string('tp_category'); //type of tour
            $table->decimal('tp_price'); //price of the tour package
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_packages');
    }
}
