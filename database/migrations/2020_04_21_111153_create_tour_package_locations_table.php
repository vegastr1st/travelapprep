<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTourPackageLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_package_locations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tp_id')->unsigned();
            $table->string('tp_loc1')->nullable();; //set location of the tour package
            $table->string('tp_loc2')->nullable();; //set location of the tour package
            $table->string('tp_loc3')->nullable();; //set location of the tour package
            $table->string('tp_loc4')->nullable();; //set location of the tour package
            $table->string('tp_loc5')->nullable();; //set location of the tour package
            $table->string('tp_loc6')->nullable();; //set location of the tour package
            $table->string('tp_loc7')->nullable();; //set location of the tour package
            $table->string('tp_loc8')->nullable();; //set location of the tour package
            $table->string('tp_loc9')->nullable();; //set location of the tour package
            $table->string('tp_loc10')->nullable();; //set location of the tour package
            $table->foreign('tp_id')->references('id')->on('tour_packages');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_package_locations');
    }
}
