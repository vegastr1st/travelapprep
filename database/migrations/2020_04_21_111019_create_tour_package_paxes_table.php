<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTourPackagePaxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_package_paxes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tp_id')->unsigned();
            $table->boolean('tp_adult')->default(1)->nullable();; //adult if enable or not
            $table->boolean('tp_child')->default(1)->nullable();; //child if enable or not
            $table->boolean('tp_infant')->default(1)->nullable();; //infant if enable or not
            $table->foreign('tp_id')->references('id')->on('tour_packages');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_package_paxes');
    }
}
