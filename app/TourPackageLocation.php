<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TourPackageLocation extends Model
{
    protected $fillable = [
        'tp_id',
        'tp_loc1',
        'tp_loc2',
        'tp_loc3',
        'tp_loc4',
        'tp_loc5',
        'tp_loc6',
        'tp_loc7',
        'tp_loc8',
        'tp_loc9',
        'tp_loc10'
    ];//
}
