<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TourPackageTransport extends Model
{
    protected $fillable = [
        'tp_id',
        'tp_cab',
        'tp_car',
        'tp_bus',
        'tp_van',
        'tp_coasterbus',
        'tp_flight',
        'tp_cruise'
    ];//
}
