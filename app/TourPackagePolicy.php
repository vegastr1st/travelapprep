<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TourPackagePolicy extends Model
{
    protected $fillable = [
        'tp_id',
        'tp_policy_type',
        'tp_coverage',
        'tp_policy_price',
        'tp_deductible',
        'tp_issued_date'
    ];//
}
