<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TourPackagePax extends Model
{
    protected $fillable = [
        'tp_id',
        'tp_adult',
        'tp_child',
        'tp_infant'
    ];//
}
