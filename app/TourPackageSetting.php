<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TourPackageSetting extends Model
{
    protected $fillable = [
        'tp_id',
        'tp_rating',
        'tp_day',
        'tp_night',
        'tp_vat',
        'tp_tax'
    ];//
}
