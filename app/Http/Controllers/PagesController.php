<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index(){
        $title = 'Welcome to Laravel!!!!';
        // return view('pages.index', compact('title'));
        return view("pages.index")->with('title', $title);
    }

    public function touroperator(){
        $title = 'Welcome to Laravel!!!!';
        return view("pages.touroperator")->with('title', $title);
    }

    public function about(){
        $title = 'About';
        return view("pages.about")->with('title', $title);
    }

    public function services(){
        $data = array(
            'title' => 'Services Title',
            'services' => ['web Design', 'Programming', 'SEO']
        );
        return view("pages.services")->with($data);
    }

    public function login(){
        $title = 'LOGIN Panel';
        return view("pages.connect.login")->with('title', $title);
    }

    public function signup(){
        $title = 'SIGN Up Panel';
        return view("pages.signup")->with('title', $title);
    }

    public function manager(){
        $title = 'SIGN Up Panel';
        return view("pages.manager")->with('title', $title);
    }

    public function contactus(){
        $title = 'SIGN Up Panel';
        return view("pages.contactus")->with('title', $title);
    }

    // public function backup(){
    //     $title = 'Sample Panel';
    //     return view("pages.index")->with('title', $title);
    // }

}