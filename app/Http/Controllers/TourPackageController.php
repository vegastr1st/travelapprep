<?php

namespace App\Http\Controllers;

use App\TourPackage;
use App\TourPackagePax;
use App\TourPackageLocation;
use App\TourPackagePolicy;
use App\TourPackageSetting;
use App\TourPackageTransport;
use Illuminate\Http\Request;
use DB; //to access database query

class TourPackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $tourpackage = TourPackage::all();
        // // // return view('pages.index2', compact('tourpackages'));//
        // return view('pages.touroperator.create');
    }

    //this function use in tour operator
    public function tourpackageslist()
    {
         $tourpackage = TourPackage::all();
        // dd($tourpackage);
        return view('pages.touroperator.index', [
            'tourpackages' => $tourpackage,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=$request->all();
        // dd($data);
        $lastid=TourPackage::create($data)->id;
        // dd($lastid);
        $tourpackagepax = new TourPackagePax([
            'tp_id'=>$lastid,
            'tp_adult' => $request->get('tp_adult') ?? 0,
            'tp_child' => $request->get('tp_child') ?? 0,
            'tp_infant' => $request->get('tp_infant') ?? 0
        ]);
        $tourpackagepax -> save();
        // dd($tourpackagepax);

        $tourpackagelocation = new TourPackageLocation([
            'tp_id'=>$lastid,
            'tp_loc1' => $request->get('tp_loc1'),
            'tp_loc2' => $request->get('tp_loc2'),
            'tp_loc3' => $request->get('tp_loc3'),
            'tp_loc4' => $request->get('tp_loc4'),
            'tp_loc5' => $request->get('tp_loc5'),
            'tp_loc6' => $request->get('tp_loc6'),
            'tp_loc7' => $request->get('tp_loc7'),
            'tp_loc8' => $request->get('tp_loc8'),
            'tp_loc9' => $request->get('tp_loc9'),
            'tp_loc10' => $request->get('tp_loc10'),
        ]);
        $tourpackagelocation -> save();
        // dd($tourpackagelocation);

        $tourpackagetransport = new TourPackageTransport([
            'tp_id'=>$lastid,
            'tp_cab' => $request->get('tp_cab') ?? 0,
            'tp_car' => $request->get('tp_car') ?? 0,
            'tp_bus' => $request->get('tp_bus') ?? 0,
            'tp_van' => $request->get('tp_van') ?? 0,
            'tp_coasterbus' => $request->get('tp_coasterbus') ?? 0,
            'tp_flight' => $request->get('tp_flight') ?? 0,
            'tp_cruise' => $request->get('tp_cruise') ?? 0,
        ]);
        $tourpackagetransport -> save();
        // dd($tourpackagetransport);

        $tourpackagesetting = new TourPackageSetting([
            'tp_id'=>$lastid,
            'tp_rating' => $request->get('tp_rating'),
            'tp_day' => $request->get('tp_day'),
            'tp_night' => $request->get('tp_night'),
            'tp_vat' => $request->get('tp_vat'),
            'tp_tax' => $request->get('tp_tax')
        ]);
        $tourpackagesetting -> save();
        // dd($tourpackagesetting);
        
        $tourpackagepolicy = new TourPackagePolicy([
            'tp_id'=>$lastid,
            'tp_policy_type' => $request->get('tp_policy_type'),
            'tp_coverage' => $request->get('tp_coverage'),
            'tp_policy_price' => $request->get('tp_policy_price'),
            'tp_deductible' => $request->get('tp_deductible'),
            'tp_issued_date' => $request->get('tp_issued_date')
        ]);
        $tourpackagepolicy -> save();
        // dd($tourpackagepolicy);
        //inclusion
        //exclusion

        return back();
        // return redirect()->route('pages.index2')->with('success', 'Data Added');
        return redirect()->route('pages.touroperator')->with('success', 'Data Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // return TourPackage::find($id);
        $viewtourpackage = TourPackage::find($id);
        return view('pages.touroperator.show')->with('viewtourpackages', $viewtourpackage);
    }

    // public function productdetail($id)
    // {
    //     // return TourPackage::find($id);
    //     $viewtourpackage = TourPackage::find($id);
    //     return view('pages.posts.show')->with('viewtourpackages', $viewtourpackage);
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tp = TourPackage::find($id);
        $tppax = TourPackagePax::find($id);
        return view('pages.touroperator.edit', ['edittourpackages' => $tp, 'tppass' => $tppax]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tourpackage = TourPackage::find($id); //can save updated data
        $data=$request->all();  //can save updated data
        $tourpackage->fill($data)->save();  //can save updated data
        // dd($tourpackage);

        $tourpackagepax = TourPackagePax::find($id);
        $data = $request->all();
        $tourpackagepax->fill($data)->save();
        // dd($tourpackagepax);

        $tourpackagelocation = TourPackageLocation::find($id);
        $data = $request->all();
        $tourpackagelocation->fill($data)->save();
        // dd($tourpackagelocation);

        $tourpackagetransport = TourPackageTransport::find($id);
        $data = $request->all();
        $tourpackagetransport->fill($data)->save();
        // dd($tourpackagetransport);

        $tourpackagesetting = TourPackageSetting::find($id);
        $data = $request->all();
        $tourpackagesetting->fill($data)->save();
        // dd($tourpackagesetting);

        $tourpackagepolicy = TourPackagePolicy::find($id);
        $data = $request->all();
        $tourpackagepolicy->fill($data)->save();
        // dd($tourpackagepolicy);

        //inclusion
        //exclusion

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tourpackagepolicy = TourPackagePolicy::find($id);
        if ($tourpackagepolicy != null) {
            $tourpackagepolicy->delete();
        }

        $tourpackagesetting = TourPackageSetting::find($id);
        if ($tourpackagesetting != null) {
            $tourpackagesetting->delete();
        }

        $tourpackagetransport = TourPackageTransport::find($id);
        if ($tourpackagetransport != null) {
            $tourpackagetransport->delete();
        }

        $tourpackagelocation = TourPackageLocation::find($id);
        if ($tourpackagelocation != null) {
            $tourpackagelocation->delete();
        }

        $tourpackagepax = TourPackagePax::find($id);
        if ($tourpackagepax != null) {
            $tourpackagepax->delete();
        }

        $tourpackages = TourPackage::find($id);
        if ($tourpackages != null) {
            $tourpackages->delete();
        }
        return back();
        return redirect()->route('pages.touroperator')->with('success', 'Tour Package Removed');
    }
}
