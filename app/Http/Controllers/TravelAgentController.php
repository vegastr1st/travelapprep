<?php

namespace App\Http\Controllers;

use App\TourPackage;
use App\TourPackagePax;
use App\TourPackageLocation;
use App\TourPackagePolicy;
use App\TourPackageSetting;
use App\TourPackageTransport;
use Illuminate\Http\Request;
use DB; //to access database query

class TravelAgentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return TourPackage::all();
        // return view('pages.index');
        // $testtourpackage = TourPackage::orderBy('tp_name', 'asc')->get(); //ascending order by tp_name
        // $testtourpackage = TourPackage::orderBy('tp_name', 'desc')->get(); //descending order by tp_name
        // return TourPackage::where('tp_name', 'Miyakojima - Naha')->get(); //search tour package by tp_name
        // $testtourpackage = DB::select('SELECT * FROM tour_packages'); //Can be use or the other one V (next line)
        // $testtourpackage = TourPackage::orderBy('tp_name', 'desc')->take(3)->get(); //take in list of tour package only 3 or (depend)
        // $testtourpackage = TourPackage::orderBy('id', 'asc')->take(3)->get(); //take in list of tour package only 3 or (depend)
        // $testtourpackage = TourPackage::orderBy('tp_name', 'asc')->paginate(3); //making paginate (1-or more) //put this on .blade.php {{$testtourpackages->links()}}
        // $testtourpackage = TourPackage::all();

        $jointest = DB::table('tour_packages')
            ->join('tour_package_settings', 'tour_packages.id', '=', 'tour_package_settings.tp_id')->take(3)
            // ->join('orders', 'users.id', '=', 'orders.user_id')
            // ->select('users.*', 'contacts.phone', 'orders.price')
            ->get();
        // dd($jointest);
        return view('pages.index')->with('jointests', $jointest);

        // $testtourpackage = TourPackage::orderBy('id', 'asc')->take(3)->get();
        //     // $testtourpackagesetting = TourPackageSetting::all();
        // $testtourpackagesetting = TourPackageSetting::orderBy('id', 'asc')->take(3)->get();
        //     // dd($testtourpackagesetting);
        // return view('pages.index')->with('testtourpackages', $testtourpackage)
        //     ->with('testtourpackagesettings', $testtourpackagesetting);
        //     // return view('pages.index', ['testtourpackages' => $testtourpackage, 'testtourpackagesettings' => $testtourpackagesetting]);
        //     // return view('pages.index', compact($testtourpackage, $testtourpackagesetting));
        //     // return view('profile.index',['profile' => $profile, 'education' => $education]); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // return TourPackage::find($id);
        $viewtourpackage = TourPackage::find($id);
        return view('pages.travelagent.show')->with('viewtourpackages', $viewtourpackage);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
