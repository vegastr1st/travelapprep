<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TourPackage extends Model
{
    protected $fillable = [
        'tp_name',
        'tp_description',
        'tp_start',
        'tp_end',
        'tp_departure',
        'tp_destination',
        'tp_category',
        'tp_price'
    ];//
}